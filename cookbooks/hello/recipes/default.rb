# -*- coding: utf-8 -*-


#git "/root/bitbucket_test" do
#  repository "git@bitbucket.org:htakeuchi0216/chef-cookbook.git"
#  ssh_wrapper "/root/.ssh/ssh4bitbucket.sh"
#end

home_dir = "/root"
user     = "root"#"line"
group    = "root"#"line"

# line用ユーザー、グループ作成
# password作成コマンド:
# openssl passwd -1 'password'
user "line" do
  password "$1$azfQFDG5$iWWu2Zlh6w3Z0.sQBSk1B1"
  supports :manage_home => true
  action :create
end

group "line" do
  action [:modify]
  members ["line"]
  append true
end


directory "/root/.ssh" do
  action :create
  mode   "700"
end
cookbook_file "/root/.ssh/id_rsa" do
  source "id_rsa"
  mode   "600"
end
file "/root/.ssh/ssh4bitbucket.sh" do
  mode "700"
  content <<-EOL
    #!/bin/bash
    ssh -i /root/.ssh/id_rsa -o "StrictHostKeyChecking=no" "$@"
  EOL
end

#install 
%w{make gcc zlib-devel openssl-devel emacs
   readline-devel ncurses-devel gdbm-devel db4-devel libffi-devel libyaml-devel
   libxml2 libxslt libxml2-devel libxslt-devel php-mysql
   ruby-devel git rubygems gcc-c++ mysql-devel sqlite-devel}.each do |pkg| 
    package pkg do
        action :install
    end
end

## php-mcryptパッケージのインストール
#package "php-mcrypt" do
#  action :install
#  options "--enablerepo=epel,remi,rpmforge"
#end

execute "devtools" do
  user "root"
  command 'yum -y groupinstall "Development Tools"'
  action :run
end

execute "Web Server" do
  user "root"
  command 'yum -y groupinstall "Web Server"'
  action :run
end

execute "MYSQL Database server" do
  user "root"
  command 'yum -y groupinstall "MySQL Database"'
  action :run
end

execute "PHP Support" do
  user "root"
  command 'yum -y groupinstall "PHP Support"'
  action :run
end

execute "Japanese Support" do
  user "root"
  command 'yum -y groupinstall "Japanese Support"'
  action :run
end


# httpd install and boot
# httpd.conf設定
template "httpd.conf" do
  path "/etc/httpd/conf/httpd.conf"
  source "httpd.conf.erb"
  mode 0644
  action :nothing
end

# パッケージインストール
package "httpd" do
  action :install
  notifies :create, resources( :template => "httpd.conf" )
end

execute "chkconfig httpd on" do
  command "chkconfig httpd on"
end

# サービス起動と自動起動設定
service "httpd" do
  action [:start, :enable]
end

# mysql install and boot
package "mysql-server" do
  action :install
end

service "mysqld" do
  action [:enable, :start]
end

#ソースコードの配置
dirpath = "/var/www/html"
line_dirpath = "/var/www/html"
directory dirpath do
    owner   user
    group   group
    mode    '0755'
    action  :create
end

git "#{dirpath}/cakephp" do
  repository "git@bitbucket.org:htakeuchi0216/linetool.git"
  ssh_wrapper "/root/.ssh/ssh4bitbucket.sh"
end


# DB設定
create_db_sql = "#{dirpath}/cakephp/sql/create_db.sql"
ddlpath = "#{dirpath}/cakephp/sql/ddl.sql" 
execute "mysql-create-user" do
  command "/usr/bin/mysql -u root  < #{create_db_sql}"
end
execute "mysql-create-table" do
  command "/usr/bin/mysql -u root  line < #{ddlpath}"
end

# sudo chmod -R 707 /var/www/html/cakephp/app/tmp
 
#template "/tmp/grants.sql" do
#    owner "root"
#    group "root"
#    mode "0600"
#    variables(
#        :user     => node['example']['db']['user'],
#        :password => node['example']['db']['pass'],
#        :database => node['example']['db']['database']
#    )
#    notifies :run, "execute[mysql-create-user]", :immediately
#end


# rbenv install
if !File.exist?( "#{home_dir}/.rbenv" )
  git "#{home_dir}/.rbenv" do
    repository "https://github.com/sstephenson/rbenv.git"
    revision   "master"
    user       user
    group      group
    action     :sync
  end
end

#https://mistymagich.wordpress.com/2013/10/08/db%E3%81%8C%E5%AD%98%E5%9C%A8%E3%81%97%E3%81%AA%E3%81%84%E3%81%A8%E3%81%8D%E3%81%ABdb%E3%82%92%E4%BD%9C%E6%88%90%E3%81%99%E3%82%8Bchef%E3%83%AC%E3%82%B7%E3%83%94%E3%82%92%E4%BD%9C%E3%81%A3%E3%81%9F/
# mysql install & setting
package "mysql-server" do
  action :install
end
 
service "mysqld" do
    action [ :enable, :start ]
    supports :status => true, :restart => true, :reload => true
end


#timezone設定
# XXX

#XXX cron設定
#cron "name_of_cron_entry" do
#  hour 8
#  weekday 6
#  mailto admin@opscode.com
#  action :create
#end


# httpd install and boot
# httpd.conf設定
template "/etc/yum.repos.d/qt48.repo" do
  source "qt48.repo"
  mode  644
  owner "root"
  group "root"
  action :create
end

execute "qt48-qt-webkit-devel" do
  user "root"
  command 'yum install -y qt48-qt-webkit-devel
sudo ln -s /opt/rh/qt48/root/usr/include/QtCore/qconfig-64.h  /opt/rh/qt48/root/usr/include/QtCore/qconfig-x86_64.h
source /opt/rh/qt48/enable
export PATH=/opt/rh/qt48/root/usr/lib64/qt4/bin/${PATH:+:${PATH}} '
  action :run
end


#repo_path = "/etc/yum.repos.d/qt.repo"
#if !File.exist?( repo_path )
#  bash 'cakephp.tar' do
#    code <<-EOH#
#
#  touch /etc/yum.repos.d/qt.repo
#  yum -y install qt48-qt-webkit-devel
#EOH
#  end
#end

service "httpd" do
  service_name "httpd"
  restart_command "service httpd restart"
end
